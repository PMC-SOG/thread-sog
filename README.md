# PMC-SOG

(P)arallel (M)odel (C)hecking using the (S)ymbolic (O)bservation (G)raph

A Symbolic Observation Graph software tool has been implemented in C/C++.
The user can choose between sequential or parallel construction.

The [BuDDy](http://buddy.sourceforge.net/manual/main.html) BDD package and [Sylvan](https://trolando.github.io/sylvan/) package are exploited in order to represent aggregates compactly.

BuDDy used for sequential version and Sylvan for both sequential and parallel construction.

# Thread-SOG
## Description
This repository hosts the experiments and results for the Multi-threaded approach for the SOG construction and provides a short guide on how to install the tools and reproduce the results.

  ** Pthread : POSIX thread librarie.  Mutexes (Pthread lock functionality) are used to prevent data inconsistencies due to race conditions.

All experiments were performed on a [Magi cluster](http://www.univ-paris13.fr/calcul/wiki/) of Paris 13 university. 


## Building

- `git clone --recursive https://depot.lipn.univ-paris13.fr/PMC-SOG/thread-sog.git`

- `mkdir build`

- `cd build`

- `cmake ..`

- `cmake --build .`


## Testing
./thread-sog arg1 arg2 arg3 arg4
arg1: specifies method of creating threads. It can be set with one of the following values:
     * p : using pthread library
     * pc : using pthread library and applying canonization on nodes
     * l : using lace framework
     * lc : using lace framework and applying canonization on nodes
arg2: specifies the number of threads/workers to be created*
arg3: specifies the net to build its SOG
arg4: specifies the set of observable transitions

philo5 example using  12 threads and using lace framework 
```
./thread-sog l 12 philo5.net Obs5_philo 32
```
## Publications
1. [A Parallel Construction of the Symbolic Observation Graph: the Basis for Efficient Model Checking of Concurrent Systems 2017](https://www.researchgate.net/publication/315840512_A_Parallel_Construction_of_the_Symbolic_Observation_Graph_the_Basis_for_Efficient_Model_Checking_of_Concurrent_Systems)


2. [Parallel Symbolic Observation Graph 2017](https://ieeexplore.ieee.org/document/8367348)
