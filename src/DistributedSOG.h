#ifndef DISTRIBUTEDSOG_H
#define DISTRIBUTEDSOG_H
// #include "RdPBDD.h"
#include <stack>
#include <vector>
// #include "MDD.h"
//#include "MDGraph.h"
//#include "bvec.h"
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "LDDGraph.h"
#include "Net.hpp"
#include "TransSylvan.h"

typedef pair<LDDState *, MDD> couple;
typedef pair<couple, Set> Pair;
typedef stack<Pair> pile;
// typedef vector<Trans> vec_trans;
class DistributedSOG {
 public:
  DistributedSOG(const net &, int BOUND = 32, int nbThread=2,bool uselace=false,bool init = false);
  void buildFromNet(int index);
  void computeDSOG(LDDGraph &g);
  void computeSeqSOG(LDDGraph &g);
  virtual ~DistributedSOG();
  static void printhandler(ostream &o, int var);
  static void *threadHandler(void *context);
  void *doCompute();
  unsigned int getPlacesCount();
  /********************/
  MDD ImageForward(MDD From);
  void computeSOGLace(LDDGraph &g);
 protected:
 private:
  LDDGraph *m_graph;
  MDD LDDAccessible_epsilon(MDD *m);
  MDD Accessible_epsilon(MDD From);
  Set firable_obs(MDD State);
  MDD get_successor(MDD From, int t);
  int minCharge();
  bool isNotTerminated();
  //-----Original defined as members
  vector<class Transition> transitions;
  Set Observable;
  Set NonObservable;
  map<string, int> transitionName;
  Set InterfaceTrans;
  Set Formula_Trans;
  unsigned int m_nb_places;
  MDD M0;
  LDDState m_M0;
  MDD currentvar;
  MDD Canonize(MDD s, int level);
  int isSingleMDD(MDD mdd);

  // vector<TransSylvan> m_relation;
  //        vec_trans m_tb_relation[16];

  //-----------------
  vector<TransSylvan> m_tb_relation;
  int m_NbIt;
  int m_itext, m_itint;
  int m_MaxIntBdd;
  MDD *m_TabMeta;
  int m_nbmetastate;
  double m_old_size;

  pile m_st[16];
  // int m_terminaison;
  int m_charge[16];
  bool m_terminaison[16];

  int m_nb_thread;

  int m_min_charge;

  net m_net;
  int m_bound, m_init;
  int m_id_thread;
  pthread_mutex_t m_mutex;
  pthread_mutex_t m_graph_mutex;

  // int nbPlaces;

  pthread_mutex_t m_mutex_stack[16];
  pthread_spinlock_t m_spin_stack[16];

  pthread_t m_list_thread[16];
};

#endif  // DISTRIBUTEDSOG_H
