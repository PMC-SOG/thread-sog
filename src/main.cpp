// version 0.1
#include <time.h>
#include <chrono>
#include <iostream>
#include <string>

#include "DistributedSOG.h"
#include "LDDGraph.h"
#include "Net.hpp"
#include "RdPBDD.h"
#include "bdd.h"
#include "fdd.h"

using namespace std;

int Formula_transitions(const char* f, Set_mot& formula_trans, net Rv);
bdd EmersonLey(bdd, net);
double getTime() { return (double)clock() / (double)CLOCKS_PER_SEC; }
int Menu() {
  int choix;
  cout << "\t\t______________________________________________" << endl;
  cout << "\t\t|                                            |" << endl;
  cout << "\t\t|        OBSERVATION GRAPHE TOOL             |" << endl;
  cout << "\t\t______________________________________________\n\n" << endl;
  cout << "\tConstruction de l'espace d'etats accessible avec OBDDs            "
          ": 1\n"
       << endl;
  cout << "\tConstruction du graphe d'observation avec Canonize Dichotomique   "
          ": 2\n"
       << endl;
  cout << "\tProduit synchronise a la volee de n graphes d'observations        "
          ": 3\n"
       << endl;
  cout << "\tProduit synchronise canonise de n graphes d'observations          "
          ": 4\n"
       << endl;
  cout << "\tConstruction du graphe d'observation avec LDDs                    "
          ": 5\n"
       << endl;
  cout << "\tConstruction distribu�e du graphe d'observation avec LDDs         "
          ": 6\n"
       << endl;
  cout << "\tQuitter l'outil : 0" << endl;
  cin >> choix;
  return choix;
}

void reordering(int nbvar) {
  // all the variables belong to a main block
  bdd_varblockall();
  // to each fdd variable corresponds a block
  for (int i = 0; i < nbvar; i++) fdd_intaddvarblock(i, i, BDD_REORDER_FIXED);

  bdd_autoreorder(BDD_REORDER_SIFT);

  bdd_reorder_verbose(2);
}

/***********************************************/
int main(int argc, char** argv) {
  int b = 32;
  if (argc < 4) {
    cout<<"Incorrect number of arguments..."<<endl;
    cout<<"Program will exit"<<endl;
    return 0;
   }
  char Obs[100] = "";
  char Int[100] = "";
  int nbthread=atoi(argv[2])>=2?atoi(argv[2]):0;
  if (argc > 5) strcpy(Obs, argv[4]);
  if (argc > 6) strcpy(Int, argv[5]);
  b = atoi(argv[argc - 1]);
  cout << "Fichier net : " << argv[3] << endl;
  cout << "Fichier formule : " << Obs << endl;
  cout << "Fichier Interface : " << Int << endl;
  cout << "______________________________________\n";
  net R(argv[3], Obs, Int);

  // cout<<"______________Apres construction de net________________________\n";
  // cout<<R<<endl;
  double d, tps;

  LDDGraph g;
  cout << "LDD Graph construction to be started...!!!" << endl;
  auto t1=std::chrono::high_resolution_clock::now(),t2=std::chrono::high_resolution_clock::now();
  if (!strcmp(argv[1],"p")) {
    cout<<"Construction with pthread library."<<endl;
    cout<<"Count of threads to be created: "<<nbthread<<endl;
    DistributedSOG DR(R, b,nbthread);

    t1 = std::chrono::high_resolution_clock::now();
    DR.computeDSOG(g);
    t2 = std::chrono::high_resolution_clock::now();

  }
  else if (!strcmp(argv[1],"pc")) {
    cout<<"Canonized construction with pthread library."<<endl;
    cout<<"Count of threads to be created: "<<nbthread<<endl;
    DistributedSOG DR(R, b,nbthread);
    t1 = std::chrono::high_resolution_clock::now();
    DR.computeDSOG(g);
    t2 = std::chrono::high_resolution_clock::now();

  } else if (!strcmp(argv[1],"l")) {
    cout<<"Construction with lace framework."<<endl;
    cout<<"Count of workers to be created: "<<nbthread<<endl;
    DistributedSOG DR(R, b,nbthread,true);
    t1 = std::chrono::high_resolution_clock::now();
    DR.computeSOGLace(g);
    t2 = std::chrono::high_resolution_clock::now();
  }
  else {
    cout<<"Canonized construction with lace framework."<<endl;
    cout<<"Count of workers to be created: "<<nbthread<<endl;
    DistributedSOG DR(R, b,nbthread,true);
    t1 = std::chrono::high_resolution_clock::now();
    DR.computeSOGLace(g);
    t2 = std::chrono::high_resolution_clock::now();
    }
  std::cout
      << "temps de construction du graphe d'observation "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
      << " milliseconds\n";
  // tps = getTime() - d;
  // cout << " Temps de construction du graphe d'observation " << tps << endl;
  g.printCompleteInformation();

  return 1;
}
