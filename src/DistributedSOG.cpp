#include "DistributedSOG.h"
#include <sylvan_sog.h>

// #include <vec.h>


// #include "test_assert.h"
#include <sylvan.h>



/*******************************************MDD node structur*********************************/
typedef struct __attribute__((packed)) mddnode
{
    uint64_t a, b;
} * mddnode_t; // 16 bytes

// RmRR RRRR RRRR VVVV | VVVV DcDD DDDD DDDD (little endian - in memory)
// VVVV RRRR RRRR RRRm | DDDD DDDD DDDc VVVV (big endian)

// Ensure our mddnode is 16 bytes
typedef char __lddmc_check_mddnode_t_is_16_bytes[(sizeof(struct mddnode)==16) ? 1 : -1];

static inline uint32_t __attribute__((unused))
mddnode_getvalue(mddnode_t n)
{
    return *(uint32_t*)((uint8_t*)n+6);
}

static inline uint8_t __attribute__((unused))
mddnode_getmark(mddnode_t n)
{
    return n->a & 1;
}

static inline uint8_t __attribute__((unused))
mddnode_getcopy(mddnode_t n)
{
    return n->b & 0x10000 ? 1 : 0;
}

static inline uint64_t __attribute__((unused))
mddnode_getright(mddnode_t n)
{
    return (n->a & 0x0000ffffffffffff) >> 1;
}

static inline uint64_t __attribute__((unused))
mddnode_getdown(mddnode_t n)
{
    return n->b >> 17;
}

#define GETNODE(mdd) ((mddnode_t)llmsset_index_to_ptr(nodes, mdd))


/***********************************************************************/


//#include <boost/thread.hpp>
const vector<class Place> *_vplaces = NULL;

/*void my_error_handler_dist(int errcode) {
    cout<<"errcode = "<<errcode<<endl;
	if (errcode == BDD_RANGE) {
		// Value out of range : increase the size of the variables...
		// but which one???
		bdd_default_errhandler(errcode);
	}
	else {
		bdd_default_errhandler(errcode);
	}
} */



int  a, b, i, tid;
float x;





DistributedSOG::DistributedSOG(const net &R, int BOUND, int nbThread,bool uselace,bool init)
{


    m_nb_thread=nbThread;
    if (uselace)  {
    lace_init(m_nb_thread, 10000000);
    }
    else lace_init(1, 10000000);

    lace_startup(0, NULL, NULL);

    LACE_ME;
    // Simple Sylvan initialization, also initialize MDD support
    sylvan_init_package(1LL<<27, 1LL<<27, 1LL<<20, 1LL<<22);
    //sylvan_init_bdd(1);
    sylvan_init_ldd();

    sylvan_gc_enable();

    m_net=R;

    m_init=init;
    int nbPlaces=R.places.size(), i, domain;
    vector<Place>::const_iterator it_places;
    //_______________
    transitions=R.transitions;
    Observable=R.Observable;
    NonObservable=R.NonObservable;
    Formula_Trans=R.Formula_Trans;
    transitionName=R.transitionName;
    InterfaceTrans=R.InterfaceTrans;
    m_nb_places=R.places.size();
    cout<<"Nombre de places : "<<m_nb_places<<endl;
    cout<<"Derniere place : "<<R.places[m_nb_places-1].name<<endl;
    // place domain, place bvect, place initial marking and place name
    // domains


    // bvec


    // Computing initial marking

    uint32_t * liste_marques=new uint32_t[R.places.size()];
    for(i=0,it_places=R.places.begin(); it_places!=R.places.end(); i++,it_places++)
    {
        liste_marques[i] =it_places->marking;
    }
    M0=lddmc_cube(liste_marques,R.places.size());

    // place names
    _vplaces = &R.places;


    uint32_t *prec = new uint32_t[nbPlaces];
    uint32_t *postc= new uint32_t [nbPlaces];
    // Transition relation
    for(vector<Transition>::const_iterator t=R.transitions.begin();
            t!=R.transitions.end(); t++)
    {
        // Initialisation
        for(i=0; i<nbPlaces; i++)
        {
            prec[i]=0;
            postc[i]=0;
        }
        // Calculer les places adjacentes a la transition t
        set<int> adjacentPlace;
        for(vector< pair<int,int> >::const_iterator it=t->pre.begin(); it!=t->pre.end(); it++)
        {
            adjacentPlace.insert(it->first);
            prec[it->first] = prec[it->first] + it->second;
            //printf("It first %d \n",it->first);
            //printf("In prec %d \n",prec[it->first]);
        }
        // arcs post
        for(vector< pair<int,int> >::const_iterator it=t->post.begin(); it!=t->post.end(); it++)
        {
            adjacentPlace.insert(it->first);
            postc[it->first] = postc[it->first] + it->second;
        }

        MDD _minus=lddmc_cube(prec,nbPlaces);
        MDD _plus=lddmc_cube(postc,nbPlaces);
        m_tb_relation.push_back(TransSylvan(_minus,_plus));
    }



    delete [] prec;
    delete [] postc;

}



/*********************************** Version s�quentielle en utilisant les LDD*****************************/
void DistributedSOG::computeSeqSOG(LDDGraph &g)
{
    // m_graph=&g;

    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    int nb_failed=0;
    cout<<"COMPUTE CANONICAL DETERMINISTIC GRAPH_________________________ \n";
    //cout<<"nb MDD var : "<<bdd_varnum()<<endl;
    double d,tps;
    d=(double)clock() / (double)CLOCKS_PER_SEC;

    double old_size;

    m_nbmetastate=0;
    m_MaxIntBdd=0;
    typedef pair<LDDState*,MDD> couple;
    typedef pair<couple, Set> Pair;
    typedef stack<Pair> pile;
    pile st;
    m_NbIt=0;
    m_itext=m_itint=0;
    LDDState* reached_class;
    Set fire;
    //FILE *fp=fopen("test.dot","w");
    // size_t max_meta_state_size;
    LDDState *c=new LDDState;
    {

        // cout<<"Marquage initial :\n";
        //cout<<bddtable<<M0<<endl;
        MDD Complete_meta_state=Accessible_epsilon(M0);

        //lddmc_fprintdot(fp,Complete_meta_state);
        //fclose(fp);

        //cout<<"Apres accessible epsilon \n";
        fire=firable_obs(Complete_meta_state);

        //c->blocage=Set_Bloc(Complete_meta_state);
        //c->boucle=Set_Div(Complete_meta_state);
        c->m_lddstate=Complete_meta_state;
        //TabMeta[m_nbmetastate]=c->m_lddstate;
        m_nbmetastate++;
        old_size=lddmc_nodecount(c->m_lddstate);
        //max_meta_state_size=bdd_pathcount(Complete_meta_state);
        st.push(Pair(couple(c,Complete_meta_state),fire));
    }
    g.setInitialState(c);
    g.insert(c);
    //LACE_ME;
    g.m_nbMarking+=lddmc_nodecount(c->m_lddstate);
    do
    {
        m_NbIt++;
        //cout<<"in loop"<<endl;
        Pair  e=st.top();
        st.pop();
        m_nbmetastate--;
        while(!e.second.empty())
        {
            //cout<<"in loop while"<<endl;
            int t = *e.second.begin();
            e.second.erase(t);
            double nbnode;
            reached_class=new LDDState;
            {
                //  cout<<"Avant Accessible epsilon \n";
                MDD Complete_meta_state=Accessible_epsilon(get_successor(e.first.second,t));
                //cout<<"Apres accessible epsilon \n";
                //cout<<"Avant CanonizeR \n";
                //reached_class->m_lddstate=CanonizeR(Complete_meta_state,0);
                //cout<<"Apres CanonizeR \n";
                // cout<<"Apres CanonizeR nb representant : "<<bdd_pathcount(reached_class->m_lddstate)<<endl;
                reached_class->m_lddstate=Complete_meta_state;
                LDDState* pos=g.find(reached_class);
                //nbnode=sylvan_pathcount(reached_class->m_lddstate);
                if(!pos)
                {
                    //  cout<<"not found"<<endl;
                    //reached_class->blocage=Set_Bloc(Complete_meta_state);
                    //reached_class->boucle=Set_Div(Complete_meta_state);
                    fire=firable_obs(Complete_meta_state);
                    st.push(Pair(couple(reached_class,Complete_meta_state),fire));
                    //TabMeta[nbmetastate]=reached_class->m_lddstate;
                    m_nbmetastate++;
                    //old_size=bdd_anodecount(TabMeta,nbmetastate);
                    e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(reached_class,t));
                    reached_class->Predecessors.insert(reached_class->Predecessors.begin(),LDDEdge(e.first.first,t));
                    g.addArc();
                    g.insert(reached_class);
                }
                else
                {
                    //  cout<<" found"<<endl;
                    nb_failed++;
                    delete reached_class;
                    e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(pos,t));
                    pos->Predecessors.insert(pos->Predecessors.begin(),LDDEdge(e.first.first,t));
                    g.addArc();
                }
            }

        }
    }
    while(!st.empty());

    tps=(double)clock() / (double)CLOCKS_PER_SEC-d;
    cout<<"TIME OF CONSTRUCTIONR : "<<tps<<endl;
    cout<<" MAXIMAL INTERMEDIARY MDD SIZE \n"<<m_MaxIntBdd<<endl;
    cout<<"OLD SIZE : "<<old_size<<endl;
    //cout<<"NB SHARED NODES : "<<bdd_anodecount(TabMeta,nbmetastate)<<endl;
    cout<<"NB META STATE DANS CONSTRUCTION : "<<m_nbmetastate<<endl;
    cout<<"NB ITERATIONS CONSTRUCTION : "<<m_NbIt<<endl;
    cout<<"Nb Iteration externes : "<<m_itext<<endl;
    cout<<"Nb Iteration internes : "<<m_itint<<endl;
    cout<<"Nb failed :"<<nb_failed<<endl;
    sylvan_stats_report(stdout, 1);

}
/*************************************** Version distribu�e en utilisant les LDD************************************************/

void * DistributedSOG::doCompute()
{

    int id_thread;
    int nb_it=0,nb_failed=0,max_succ=0;
    pthread_mutex_lock(&m_mutex);
    id_thread=m_id_thread++;
    lace_init_worker(id_thread, 0);
    //cout<<"Thread number "<<id_thread<<" is started!"<<endl;
    pthread_mutex_unlock(&m_mutex);


    //pthread_mutex_lock(&m_mutex);
    //cout<<"COMPUTE CANONICAL DETERMINISTIC GRAPH_ :"<<endl;
    // pthread_mutex_unlock(&m_mutex);
    //cout<<"nb MDD var : "<<bdd_varnum()<<endl;

    Set fire;
    if (id_thread==0)
    {
        m_min_charge=0;
        m_nbmetastate=0;
        m_MaxIntBdd=0;
        m_NbIt=0;
        m_itext=m_itint=0;
        LDDState *c=new LDDState;

        //cout<<"Marquage initial is being built..."<<endl;
        // cout<<bddtable<<M0<<endl;
        MDD Complete_meta_state(Accessible_epsilon(M0));

        //cout<<"Apres accessible epsilon \n";
        fire=firable_obs(Complete_meta_state);
        //c->blocage=Set_Bloc(Complete_meta_state);
        //c->boucle=Set_Div(Complete_meta_state);
        //c->m_lddstate=CanonizeR(Complete_meta_state,0);
        //cout<<"Apres CanonizeR nb representant : "<<bdd_pathcount(c->m_lddstate)<<endl;
        c->m_lddstate=Complete_meta_state;
        //m_TabMeta[m_nbmetastate]=c->m_lddstate;
        m_nbmetastate++;
        m_old_size=lddmc_nodecount(c->m_lddstate);
        //max_meta_state_size=bdd_pathcount(Complete_meta_state);
        m_st[0].push(Pair(couple(c,Complete_meta_state),fire));
        m_graph->setInitialState(c);
        m_graph->insert(c);
        //m_graph->nbMarking+=bdd_pathcount(c->m_lddstate);
        m_charge[0]=1;

    }

    LDDState* reached_class;
    MDD Complete_meta_state;

    // size_t max_meta_state_size;
    // int min_charge;
    do
    {
        while (!m_st[id_thread].empty())
        {
            // MDD Reduce;
            // cout<<"id thread:"<<id_thread<<" : ("<<m_st[0].size()<<","<<m_st[1].size()<<","<<m_st[2].size()<<","<<m_st[3].size()<<","<<m_st[4].size()<<")"<<endl;
            //cout<<"Je suis le thread"<<id_thread<<endl;
            nb_it++;
            m_terminaison[id_thread]=false;
            pthread_spin_lock(&m_spin_stack[id_thread]);
            Pair  e=m_st[id_thread].top();
            //pthread_spin_lock(&m_accessible);
            m_st[id_thread].pop();
            //pthread_spin_unlock(&m_accessible);
            pthread_spin_unlock(&m_spin_stack[id_thread]);
            m_nbmetastate--;

            m_charge[id_thread]--;
            while(!e.second.empty())
            {
                int t = *e.second.begin();
                e.second.erase(t);
                double nbnode;
                reached_class=new LDDState;

                // cout<<e.second.size()<<endl;

                //pthread_mutex_lock(&m_mutex);
                //pthread_spin_lock(&m_accessible);
                /* double d,tps;
                d=(double)clock() / (double)CLOCKS_PER_SEC;*/
                MDD Complete_meta_state=Accessible_epsilon(get_successor(e.first.second,t));
                //    cout<<"Avant CanonizeR nb noeuds : "<<lddmc_nodecount(Complete_meta_state)<<endl;


                //pthread_mutex_unlock(&m_mutex);
                //cout<<"Apres accessible epsilon \n";
                //cout<<"Avant CanonizeR \n";

                reached_class->m_lddstate=Complete_meta_state;

                //   Reduce=Canonize(Complete_meta_state,0);
                //   cout<<"Apres Canonize \n";
                //   cout<<"Apres Canonize nb noeuds : "<<lddmc_nodecount(Reduce)<<endl;
                //nbnode=bdd_pathcount(reached_class->m_lddstate);

                //pthread_spin_lock(&m_accessible);
                pthread_mutex_lock(&m_graph_mutex);

                LDDState* pos=m_graph->find(reached_class);

                /*tps=(double)clock() / (double)CLOCKS_PER_SEC - d;
                cout<<tps<<endl;*/

                if(!pos)
                {

                    //  cout<<"not found"<<endl;
                    //reached_class->blocage=Set_Bloc(Complete_meta_state);
                    //reached_class->boucle=Set_Div(Complete_meta_state);

                    //TabMeta[nbmetastate]=reached_class->m_lddstate;
                    //old_size=bdd_anodecount(TabMeta,nbmetastate);

                    m_graph->addArc();
                    m_graph->insert(reached_class);
                    //pthread_spin_unlock(&m_accessible);
                    pthread_mutex_unlock(&m_graph_mutex);



                    e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(reached_class,t));
                    reached_class->Predecessors.insert(reached_class->Predecessors.begin(),LDDEdge(e.first.first,t));
                    m_nbmetastate++;


                    //pthread_mutex_lock(&m_mutex);
                    fire=firable_obs(Complete_meta_state);
                    if (max_succ<fire.size())
                        max_succ=fire.size();
                    //pthread_mutex_unlock(&m_mutex);
                    m_min_charge=(m_min_charge+1)%m_nb_thread;//   minCharge();

                    //m_min_charge=(m_min_charge+1) % m_nb_thread;
                    pthread_spin_lock(&m_spin_stack[m_min_charge]);
                    m_st[m_min_charge].push(Pair(couple(reached_class,Complete_meta_state),fire));
                    pthread_spin_unlock(&m_spin_stack[m_min_charge]);
                    m_charge[m_min_charge]++;
                }
                else
                {
                    //  cout<<" found"<<endl;

                    nb_failed++;
                    m_graph->addArc();
                    pthread_mutex_unlock(&m_graph_mutex);
                    //pthread_spin_unlock(&m_accessible);
                    e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(pos,t));
                    pos->Predecessors.insert(pos->Predecessors.begin(),LDDEdge(e.first.first,t));
                    //pthread_mutex_lock(&m_mutex);
                    delete reached_class;
                    //pthread_mutex_unlock(&m_mutex);

                }



            }
        }
        m_terminaison[id_thread]=true;

    }
    while (isNotTerminated());
    //  cout<<"Thread :"<<id_thread<<"  has performed "<<nb_it<<" it�rations avec "<<nb_failed<<" �checs"<<endl;
    //  cout<<"Max succ :"<<max_succ<<endl;
}
bool DistributedSOG::isNotTerminated()
{
    bool res=true;
    int i=0;
    while (i<m_nb_thread && res)
    {
        res=m_terminaison[i];
        i++;
    }
    return !res;
}
void DistributedSOG::computeDSOG(LDDGraph &g)
{
    m_nb_thread=2;
    int rc;
    m_graph=&g;
    m_id_thread=0;


    pthread_mutex_init(&m_mutex, NULL);
    pthread_mutex_init(&m_graph_mutex,NULL);
    for (int i=0; i<m_nb_thread; i++)
    {
        pthread_spin_init(&m_spin_stack[i], NULL);
        m_charge[i]=0;
        m_terminaison[i]=false;
    }


    for (int i=0; i<m_nb_thread-1; i++)
    {
        if ((rc = pthread_create(&m_list_thread[i], NULL,threadHandler,this)))
        {
            cout<<"error: pthread_create, rc: "<<rc<<endl;
        }
    }
    double d,tps;
    d=(double)clock() / (double)CLOCKS_PER_SEC;
    doCompute();
    for (int i = 0; i < m_nb_thread-1; i++)
    {
        pthread_join(m_list_thread[i], NULL);
    }
    tps=(double)clock() / (double)CLOCKS_PER_SEC-d;
    cout<<"TIME OF CONSTRUCTIONR : "<<tps<<endl;
    cout<<" MAXIMAL INTERMEDIARY MDD SIZE \n"<<m_MaxIntBdd<<endl;
    cout<<"OLD SIZE : "<<m_old_size<<endl;
    //cout<<"NB SHARED NODES : "<<bdd_anodecount(TabMeta,nbmetastate)<<endl;
    cout<<"NB META STATE DANS CONSTRUCTION : "<<m_nbmetastate<<endl;
    cout<<"NB ITERATIONS CONSTRUCTION : "<<m_NbIt<<endl;
    cout<<"Nb Iteration externes : "<<m_itext<<endl;
    cout<<"Nb Iteration internes : "<<m_itint<<endl;
    cout<<"Nb Threads : "<<m_nb_thread<<endl;
}
void * DistributedSOG::threadHandler(void *context)
{

    return ((DistributedSOG*)context)->doCompute();
}
void DistributedSOG::printhandler(ostream &o, int var)
{
    o << (*_vplaces)[var/2].name;
    if (var%2)
        o << "_p";
}










Set DistributedSOG::firable_obs(MDD State)
{
    Set res;LACE_ME;
    for(Set::const_iterator i=Observable.begin(); !(i==Observable.end()); i++)
    {
        //cout<<"firable..."<<endl;
        MDD succ = lddmc_firing_lace(State,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
        if(succ!=lddmc_false)
        {
            //cout<<"firable..."<<endl;
            res.insert(*i);
        }

    }
    return res;
}

MDD DistributedSOG::get_successor(MDD From,int t)
{
    //LACE_ME;
    return lddmc_firing_mono(From,m_tb_relation[(t)].getMinus(),m_tb_relation[(t)].getPlus());
}


MDD DistributedSOG::Accessible_epsilon(MDD From)
{
    MDD M1;
    MDD M2=From;
    int it=0;
    do
    {
        M1=M2;
        for(Set::const_iterator i=NonObservable.begin(); !(i==NonObservable.end()); i++)
        {
            //LACE_ME;
            MDD succ= lddmc_firing_mono(M2,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
            M2=lddmc_union_mono(succ,M2);
            //M2=succ|M2;
        }

        //TabMeta[nbmetastate]=M2;
        //int intsize=sylvan_anodecount(TabMeta,nbmetastate+1);
        //if(m_MaxIntBdd<intsize)
        //  m_MaxIntBdd=intsize;
        it++;
        //	cout << bdd_nodecount(M2) << endl;
    }
    while(M1!=M2);
    //cout << endl;
    return M2;
}




int DistributedSOG::minCharge()
{
    int pos=0;
    int min_charge=m_charge[0];
    for (int i=1; i<m_nb_thread; i++)
    {
        if (m_charge[i]<min_charge)
        {
            min_charge=m_charge[i];
            pos=i;
        }
    }
    return pos;
}
DistributedSOG::~DistributedSOG()
{
    //dtor
}


int DistributedSOG::isSingleMDD(MDD mdd)
{
    mddnode_t node;


    while (mdd!=lddmc_false)
    {
        node=GETNODE(mdd);
        if (mddnode_getright(node)!=lddmc_false )
            return 0;
        mdd=mddnode_getdown(node);
    }
    return 1;
}


/*********Returns the count of places******************************************/
unsigned int DistributedSOG::getPlacesCount() {
    return m_nb_places;
}
/******************************************************************************/
MDD DistributedSOG::ImageForward(MDD From)
{
    MDD Res=lddmc_false;
    //MDD Res=From;
    //MDD succ;
    for(Set::const_iterator i=NonObservable.begin(); !(i==NonObservable.end()); i++)
    {
        MDD succ= lddmc_firing_mono(From,m_tb_relation[(*i)].getMinus(),m_tb_relation[(*i)].getPlus());
        Res=lddmc_union_mono(Res,succ);
    }
    return Res;
}

/*********************** Parallel lace functions *****************************************/
TASK_3 (MDD, Accessible_epsilon_lace, MDD, From, Set*, nonObservable, vector<TransSylvan>*, tb_relation)
{
    MDD M1;
    MDD M2=From;
    int it=0;
    //cout<<"worker "<<lace_get_worker()->worker<<endl;
    do
    {
        M1=M2;
        for(Set::const_iterator i=nonObservable->begin(); !(i==nonObservable->end()); i++)
        {
            SPAWN(lddmc_firing_lace,M2,(*tb_relation)[(*i)].getMinus(),(*tb_relation)[(*i)].getPlus());

            //M2=succ|M2;
        }
        for(Set::const_iterator i=nonObservable->begin(); !(i==nonObservable->end()); i++)
        {
            MDD succ=SYNC(lddmc_firing_lace);
            M2=lddmc_union(succ,M2);
        }

    }
    while(M1!=M2);
    return M2;
}


/********************* Compute SOG with lace *********************************************/
void DistributedSOG::computeSOGLace(LDDGraph &g)
{
    Set fire;
    m_graph=&g;


    m_nbmetastate=0;
    m_MaxIntBdd=0;


    LDDState *c=new LDDState;

    //cout<<"Marquage initial is being built..."<<endl;
    LACE_ME;
    MDD initial_meta_state(CALL(Accessible_epsilon_lace,M0,&NonObservable,&m_tb_relation));


    fire=firable_obs(initial_meta_state);

    c->m_lddstate=initial_meta_state;

    m_nbmetastate++;
    m_old_size=lddmc_nodecount(c->m_lddstate);

    //max_meta_state_size=bdd_pathcount(Complete_meta_state);
    m_st[0].push(Pair(couple(c,initial_meta_state),fire));
    m_graph->setInitialState(c);
    m_graph->insert(c);
    //m_graph->nbMarking+=bdd_pathcount(c->m_lddstate);
    LDDState* reached_class;
    //  MDD Complete_meta_state;*/

    // size_t max_meta_state_size;
    // int min_charge;

    while (!m_st[0].empty())
    {

        Pair  e=m_st[0].top();
        m_st[0].pop();
        m_nbmetastate--;
        cout<<"here"<<endl;
        unsigned int onb_it=0;
        Set::const_iterator iter=e.second.begin();
        while(iter!=e.second.end())
        {
            int t = *iter;
            //e.second.erase(t);
            SPAWN(Accessible_epsilon_lace,get_successor(e.first.second,t),&NonObservable,&m_tb_relation);
            onb_it++;iter++;
        }

        for (unsigned int i=0; i<onb_it; i++)
        {
            int t = *e.second.end();
            e.second.erase(t);
            MDD Complete_meta_state=SYNC(Accessible_epsilon_lace);
            reached_class=new LDDState;
            reached_class->m_lddstate=Complete_meta_state;
            LDDState* pos=m_graph->find(reached_class);

            if(!pos)
            {

                m_graph->addArc();
                m_graph->insert(reached_class);
                e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(reached_class,t));
                reached_class->Predecessors.insert(reached_class->Predecessors.begin(),LDDEdge(e.first.first,t));
                m_nbmetastate++;
                fire=firable_obs(Complete_meta_state);
                m_st[0].push(Pair(couple(reached_class,Complete_meta_state),fire));
            }
            else
            {
                m_graph->addArc();
                e.first.first->Successors.insert(e.first.first->Successors.begin(),LDDEdge(pos,t));
                pos->Predecessors.insert(pos->Predecessors.begin(),LDDEdge(e.first.first,t));
                delete reached_class;
            }
        }
    }
    cout<<"-------------- fin -----------------"<<endl;




}
/******************************Canonizer with lace framework  **********************************/
TASK_DECL_3(MDD, lddmc_canonize,MDD, unsigned int, DistributedSOG &)
#define lddmc_canonize(s,leve,nbPlaces) CALL(lddmc_canonize, s, level,ds)


TASK_IMPL_3(MDD, lddmc_canonize,MDD, s,unsigned int, level, DistributedSOG & ,ds)
{
    if (level>ds.getPlacesCount() || s==lddmc_false) return lddmc_false;
    if(isSingleMDD(s))   return s;
    MDD s0=lddmc_false,s1=lddmc_false;

    int res=0;
    do
    {
        if (get_mddnbr(s,level)>1)
        {
            s0=ldd_divide_rec(s,level);
            s1=ldd_minus(s,s0);
            res=1;
        }
        else
            level++;
    }
    while(level<ds.getPlacesCount() && !res);


    if (s0==lddmc_false && s1==lddmc_false)
        return lddmc_false;
    // if (level==nbPlaces) return lddmc_false;
    MDD Front,Reach;
    if (s0!=lddmc_false && s1!=lddmc_false)
    {
        Front=s1;
        Reach=s1;
        do
        {
            // cout<<"premiere boucle interne \n";
            Front=lddmc_minus(ds.ImageForward(Front),Reach);
            Reach = lddmc_union(Reach,Front);
            s0 = lddmc_minus(s0,Front);
        }
        while((Front!=lddmc_false)&&(s0!=lddmc_false));
    }
    if((s0!=lddmc_false)&&(s1!=lddmc_false))
    {
        Front=s0;
        Reach = s0;
        do
        {
            //  cout<<"deuxieme boucle interne \n";
            Front=lddmc_minus(ds.ImageForward(Front),Reach);
            Reach = lddmc_union(Reach,Front);
            s1 = lddmc_minus(s1,Front);
        }
        while( Front!=lddmc_false && s1!=lddmc_false );
    }



    MDD Repr=lddmc_false;

    if (isSingleMDD(s0))
    {
        Repr=s0;
        if (isSingleMDD(s1)) {
            Repr=lddmc_union(Repr,s1);
        }
        else {
            Repr=lddmc_union(Repr,lddmc_canonize(s1,level,ds));
        }
    }
    else
    {
        SPAWN(lddmc_canonize,s0,level,ds);
        if (isSingleMDD(s1)) {
            Repr=SYNC(lddmc_canonize);
            Repr=lddmc_union(Repr,s1);
        }
        else {
            SPAWN(lddmc_canonize,s1,level,ds);
            MDD temp=SYNC(lddmc_canonize);
            Repr=SYNC(lddmc_canonize);
            Repr=lddmc_union(Repr,temp);
        }

    }
    return Repr;
}

/*----------------------------------------------CanonizeR()------------------------------------*/

/*MDD DistributedSOG::Canonize(MDD s,int level)
{
    if (level>m_nb_places || s==lddmc_false)
        return lddmc_false;

    MDD s0=lddmc_false,s1=lddmc_false;

    bool res=false;
    do
    {
        if (get_mddnbr(s,level)>1)
        {
            s0=ldd_divide_rec(s,level);
            s1=ldd_minus(s,s0);
            res=true;
        }
        else
        level++;
    }
    while(level<m_nb_places && !res);
    if (s0==lddmc_true || s1==lddmc_true) cout<<"Hooooolaaaaaaaaaaa "<<endl;

        if (s0==lddmc_false && s1==lddmc_false) return lddmc_false;
            // if (level==nbPlaces) return lddmc_false;
            MDD Front,Reach;
            if (s0!=lddmc_false && s1!=lddmc_false)
            {
                Front=s1;
                Reach=s1;
                do
                {
                    // cout<<"premiere boucle interne \n";
                    Front=ldd_minus(ImageForward(Front),Reach);
                    Reach = lddmc_union_mono(Reach,Front);
                    s0 = ldd_minus(s0,Front);
                }
                while((Front!=lddmc_false)&&(s0!=lddmc_false));
            }
            if((s0!=lddmc_false)&&(s1!=lddmc_false))
            {
                Front=s0;
                Reach = s0;
                do
                {
                    //  cout<<"deuxieme boucle interne \n";
                    Front=ldd_minus(ImageForward(Front),Reach);
                    Reach = lddmc_union_mono(Reach,Front);
                    s1 = ldd_minus(s1,Front);
                }
                while( Front!=lddmc_false && s1!=lddmc_false );
            }



        MDD Repr=lddmc_false;

        if (isSingleMDD(s0))
        {
              Repr=s0;
        }
        else
            Repr=Canonize(s0,level);
        if (isSingleMDD(s1))
            Repr=lddmc_union_mono(Repr,s1);
        else
            Repr=lddmc_union_mono(Repr,Canonize(s1,level));
        return Repr;

    } */
